package com.digitalmisfits.hurd;

import com.digitalmisfits.hurd.cluster.Cluster;
import com.digitalmisfits.hurd.cluster.ClusterInstance;
import org.apache.curator.framework.CuratorFramework;
import org.apache.curator.framework.CuratorFrameworkFactory;
import org.apache.curator.framework.state.ConnectionState;
import org.apache.curator.retry.ExponentialBackoffRetry;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Application {

    protected static final Logger LOG = LoggerFactory.getLogger(Application.class);

    static public void main(String[] args) {

        final Object lock = new Object();

        CuratorFramework curator = CuratorFrameworkFactory.newClient("localhost:2181", 16000, 8000, 
                new ExponentialBackoffRetry(1000, 3));

        curator.getConnectionStateListenable().addListener((final CuratorFramework c, final ConnectionState state) -> {
            LOG.info("Connection state {}", state.name());
        });

        LOG.info("Staring cluster");

        Cluster cluster = new Cluster(curator, ClusterInstance.SYSTEM_PROPERTY);
        cluster.start();

        LOG.info("Waiting...");

        synchronized (lock){
            try {
                lock.wait();
            } catch (InterruptedException e) {
                Thread.currentThread().interrupt();
                return;
            }
        }
    }
}
