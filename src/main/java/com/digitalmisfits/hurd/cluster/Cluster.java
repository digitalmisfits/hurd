package com.digitalmisfits.hurd.cluster;

import com.digitalmisfits.hurd.cluster.automata.StateMachine;
import com.digitalmisfits.hurd.util.HashRing;
import com.digitalmisfits.hurd.util.algo.Murmur128;
import com.google.common.base.Preconditions;
import com.google.common.collect.Lists;
import org.apache.curator.framework.CuratorFramework;
import org.apache.curator.framework.imps.CuratorFrameworkState;
import org.apache.curator.framework.recipes.cache.PathChildrenCache;
import org.apache.curator.framework.recipes.cache.PathChildrenCacheEvent;
import org.apache.curator.framework.recipes.cache.PathChildrenCacheListener;
import org.apache.curator.framework.state.ConnectionState;
import org.apache.curator.framework.state.ConnectionStateListener;
import org.apache.zookeeper.CreateMode;
import org.apache.zookeeper.KeeperException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Closeable;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.IntStream;

public class Cluster implements ConnectionStateListener, PathChildrenCacheListener, Closeable {

    protected static final Logger LOG = LoggerFactory.getLogger(Cluster.class);

    /**
     * The Apache Zookeeper Curator framework client
     */
    private final CuratorFramework curatorFramework;

    /**
     * Finite state machine to aid in handling the several connection states
     */
    private final StateMachine<ClusterLifecycleState, Void> clusterLifecycleStateMachine = new StateMachine<>();

    /**
     * Default implementation of a consistent hash ring
     */
    private HashRing ring = new HashRing(new Murmur128());

    /**
     * The ClusterInstance (e.a. this node) to be registered with ZooKeeper
     */
    private final ClusterInstance clusterInstance;

    /**
     * The default path (absolute parent) to node instances in ZooKeeper
     */
    private String path = "/instances";

    public Cluster(CuratorFramework curatorFramework, ClusterInstance clusterInstance) {
        this.curatorFramework = curatorFramework;
        this.clusterInstance = clusterInstance;

        clusterLifecycleStateMachine.addPreTransitionListener((state, previous, target) -> {
            LOG.info("Transitioning from {} to {}", previous, state);

            Preconditions.checkState(curatorFramework.getState() == CuratorFrameworkState.STARTED);
        });

        clusterLifecycleStateMachine.addStateTransitionListener(ClusterLifecycleState.CONNECTED, (state, previous, target) -> {
            // connected; inform observers
            
            registerClusterInstance();
        });

        clusterLifecycleStateMachine.addStateTransitionListener(ClusterLifecycleState.RECONNECTED, (state, previous, target) -> {
            // reconnected

            /*
                it is possible to get a {@link #ClusterLifecycleState.RECONNECTED} state
                after {@link #ClusterLifecycleState.LOST} but you should still consider any locks, etc. as dirty/unstable
             */
            if(previous == ClusterLifecycleState.LOST)
                registerClusterInstance();

        });

        clusterLifecycleStateMachine.addStateTransitionListener(ClusterLifecycleState.SUSPENDED, (state, previous, target) -> {
            // suspended; inform observers
        });

        clusterLifecycleStateMachine.addStateTransitionListener(ClusterLifecycleState.LOST, (state, previous, target) -> {
            // session expired. Inform observers and wait for connection to fully reconnect {@link #ClusterLifecycleState.CONNECTED}
        });

        curatorFramework.getConnectionStateListenable().addListener(this);
    }

    public Cluster(CuratorFramework curatorFramework, ClusterInstance clusterInstance, HashRing ring) {
        this(curatorFramework, clusterInstance);
        this.ring = ring;
    }

    /**
     * Start the cluster. Most mutator methods will not work until the cluster is started
     *
     * @throws ClusterOperationFailureException
     */
    public void start() throws ClusterOperationFailureException {

        if (curatorFramework.getState() != CuratorFrameworkState.LATENT) {
            throw new ClusterOperationFailureException("CuratorFramework cannot be manually started.");
        }

        curatorFramework.start();

        try {
            createPathChildrenCache().start(PathChildrenCache.StartMode.POST_INITIALIZED_EVENT);
        } catch (Exception e) {

            LOG.error("PathChildrenCache could not be started. Closing CuratorFramework connection");

            try {
                close();
            } catch (IOException ignore) {
                // ignore
            }

            throw new ClusterOperationFailureException("PathChildrenCache could no be started.");
        }
    }

    /**
     * Add retry policy for ephemeral node creation (when rebooting a server within the session timeout, the state
     * will be set to CONNECTED but the ephemeral node has not passed the timeout period, this currently
     * throws an exception)
     */
    private void registerClusterInstance() {

        String fullPathToInstance = Paths.get(path, clusterInstance.getNodeId()).toString();

        try {
            curatorFramework.create()
                    .creatingParentsIfNeeded().withMode(CreateMode.EPHEMERAL).forPath(fullPathToInstance);
        } catch (Exception e) {
            if (((KeeperException) e).code() == KeeperException.Code.NODEEXISTS) {
                throw new ClusterOperationFailureException("Node already exists", e);
            } else {
                e.printStackTrace();
            }
        }
    }

    public synchronized void refresh() {

        List<HashRing.VirtualNode> virtualNodes = Lists.newArrayList();

        try {
            List<String> nodes = curatorFramework.getChildren().forPath(path);
            nodes.stream().forEach(node -> {
                IntStream.range(0, 64).forEach(replica -> {
                    virtualNodes.add(new HashRing.VirtualNode(node, replica));
                });
            });

        } catch (Exception e) {
            throw new ClusterOperationFailureException("Unable to refresh hash ring", e);
        }

        ring = ring.replace(virtualNodes);
    }

    protected PathChildrenCache createPathChildrenCache() {

        PathChildrenCache cache = new PathChildrenCache(curatorFramework, path, false);
        cache.getListenable().addListener(this);
        return cache;
    }

    @Override
    public void stateChanged(CuratorFramework curatorFramework, ConnectionState state) {

        clusterLifecycleStateMachine.doTransition(ClusterLifecycleState.getLifecycleState(state), null);
    }

    @Override
    public void childEvent(CuratorFramework curatorFramework, PathChildrenCacheEvent pathChildrenCacheEvent) throws Exception {

        LOG.info("PathChildrenCacheEvent received: {}", pathChildrenCacheEvent.getType());


        if (Lists.newArrayList(
                PathChildrenCacheEvent.Type.CHILD_ADDED,
                PathChildrenCacheEvent.Type.CHILD_REMOVED,
                PathChildrenCacheEvent.Type.INITIALIZED
        ).stream().anyMatch(e -> e == pathChildrenCacheEvent.getType())) {
            refresh();
        }
    }

    @Override
    public void close() throws IOException {
        if (curatorFramework.getState() == CuratorFrameworkState.STARTED)
            curatorFramework.close();
    }
}
