package com.digitalmisfits.hurd.cluster;

public class ClusterOperationFailureException extends RuntimeException {

    public ClusterOperationFailureException(String message) {
        super(message);
    }

    public ClusterOperationFailureException(String message, Throwable cause) {
        super(message, cause);
    }
}