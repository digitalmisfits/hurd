package com.digitalmisfits.hurd.cluster;

import com.digitalmisfits.hurd.cluster.automata.EnumState;
import org.apache.curator.framework.state.ConnectionState;

import java.util.EnumSet;
import java.util.Set;

public enum ClusterLifecycleState implements EnumState<ClusterLifecycleState> {

    CONNECTED(ConnectionState.CONNECTED) {
        @Override
        public Set<ClusterLifecycleState> possibleFollowUps() {
            return EnumSet.of(SUSPENDED, LOST);
        }
    },

    RECONNECTED(ConnectionState.RECONNECTED) {
        @Override
        public Set<ClusterLifecycleState> possibleFollowUps() {
            return EnumSet.of(SUSPENDED, LOST);
        }
    },

    SUSPENDED(ConnectionState.SUSPENDED) {
        @Override
        public Set<ClusterLifecycleState> possibleFollowUps() {
            return EnumSet.of(RECONNECTED, LOST);
        }
    },

    LOST(ConnectionState.LOST) {
        @Override
        public Set<ClusterLifecycleState> possibleFollowUps() {
            return EnumSet.of(CONNECTED, RECONNECTED);
        }
    };

    private final ConnectionState state;

    ClusterLifecycleState(ConnectionState state) {
        this.state = state;
    }

    public ConnectionState getState() {
        return this.state;
    }

    public static ClusterLifecycleState getLifecycleState(ConnectionState state) {
        for (ClusterLifecycleState s : ClusterLifecycleState.values()) {
            if (s.state == state) {
                return s;
            }
        }
        return null;
    }

    @Override
    public String toString() {
        return getState().toString();
    }
}
