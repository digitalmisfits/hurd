package com.digitalmisfits.hurd.cluster.automata;


import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

public class StateMachine<T, V> {

    private EnumState<T> currentState;
    private EnumState<T> previousState;

    private final List<StateTransitionListener> preTransitionListeners = new LinkedList<>();
    private final List<StateTransitionListener> postTransitionListeners = new LinkedList<>();

    private final Map<EnumState<T>, List<StateTransitionListener>> stateListeners = new ConcurrentHashMap<>();

    public StateMachine() {
    }

    public void addPreTransitionListener(StateTransitionListener listener) {
        preTransitionListeners.add(listener);
    }

    public void addPostTransitionListener(StateTransitionListener listener) {
        postTransitionListeners.add(listener);
    }

    public void addStateTransitionListener(EnumState<T> state, StateTransitionListener listener) {

        if (!stateListeners.containsKey(state)) {
            stateListeners.put(state, new LinkedList<>());
        }

        stateListeners.get(state).add(listener);
    }

    public void doTransition(final EnumState<T> state, final V target) {

        // state transition does not occur
        if (currentState != null && currentState.equals(state)) {
            return;
        }

        if (currentState == null || currentState.possibleFollowUps().contains(state)) {

            previousState = currentState;
            currentState = state;

            preTransitionListeners.forEach(listener ->
                    listener.execute(state, previousState, target));

            if (stateListeners.containsKey(state)) {
                stateListeners.get(state).forEach(listener ->
                        listener.execute(state, previousState, target));
            }

            postTransitionListeners.forEach(listener ->
                    listener.execute(state, previousState, target));
        }
    }

    public interface StateTransitionListener<T, V> {
        public void execute(T state, T previous, V target);
    }
}