package com.digitalmisfits.hurd.util;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

public class HashRing {

    protected static final Logger LOG = LoggerFactory.getLogger(HashRing.class);

    public interface HashFunction {
        long hash(String key);
    }

    public static class VirtualNode {
        private final String nodeId;
        private final int replication;

        public VirtualNode(final String nodeId, final int replication) {
            this.nodeId = nodeId.toLowerCase();
            this.replication = replication;
        }

        public boolean matches(String host) {
            return nodeId.equalsIgnoreCase(host);
        }

        @Override
        public String toString() {
            return nodeId + ":" + replication;
        }
    }

    /**
     * HashFunction to be used for both keys and values
     */
    private final HashFunction hashFunction;

    /**
     * TreeMap/SortedMap collection of nodes in the HashRing
     */
    private final SortedMap<Long, VirtualNode> ring = new TreeMap<>();

    public HashRing(HashFunction hashFunction) {
        this.hashFunction = hashFunction;
    }

    public HashRing(HashFunction hashFunction, Collection<VirtualNode> nodes) {
        this.hashFunction = hashFunction;
        
        nodes.stream().forEach(n -> {
            add(n);
        });
    }

    /**
     * Add a virtual node to the hash ring
     * *
     * @param virtualNode
     */
    protected void add(VirtualNode virtualNode) {
       ring.putIfAbsent(hashFunction.hash(virtualNode.toString()), virtualNode);
    }

    /**
     * Return a new immutable HashRing instance with associated nodes
     * *
     * @param nodes
     * @return
     */
    public HashRing replace(Collection<VirtualNode> nodes) {
        return new HashRing(hashFunction, nodes);
    }

    /**
     * Remove a node (and all of it's replicas)
     *
     * @param nodeId
     */
    protected void remove(String nodeId) {
        throw new UnsupportedOperationException("Immutable");
    }

    /**
     * Lookup the "physhical" node based on consistent hashing
     * *
     * @param key
     * @return
     */
    public String lookup(String key) {

        if (ring.isEmpty())
            return null;

        long hash = hashFunction.hash(key);

        for (Map.Entry<Long, VirtualNode> e : ring.entrySet()) {
            if (hash < e.getKey())
                return e.getValue().nodeId;
        }

        SortedMap<Long, VirtualNode> tailMap = ring.tailMap(hash);
        hash = tailMap.isEmpty() ? ring.firstKey() : tailMap.firstKey();
        
        return ring.get(hash).nodeId;
    }
}
