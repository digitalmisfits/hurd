package com.digitalmisfits.hurd.util.algo;

import com.digitalmisfits.hurd.util.HashRing;
import com.google.common.hash.HashFunction;
import com.google.common.hash.Hashing;

import java.nio.charset.Charset;


public class Murmur128 implements HashRing.HashFunction {

    public static HashFunction murmur = Hashing.murmur3_128();

    @Override
    public long hash(String key) {
        
        return murmur.newHasher()
                .putString(key, Charset.forName("UTF-8"))
                .hash()
                .padToLong();
    }
}
